require_relative 'spec_helper'

describe NameDotComApi do

  describe "#base_url" do

    it "returns test url" do
      NameDotComApi.base_url(true).should == 'https://api.dev.name.com/v4'
    end

    it "returns production url" do
      NameDotComApi.base_url.should == 'https://api.name.com/v4'
      NameDotComApi.base_url(false).should == 'https://api.name.com/v4'
    end

  end

end