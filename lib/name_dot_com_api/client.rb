module NameDotComApi

  class Client

    attr_reader :connection

    def initialize(username, api_token, test_mode = false)
      @connection ||= Connection.new
      @connection.test_mode = test_mode 
      @connection.username ||= username
      @connection.api_token ||= api_token      
    end
  
  # https://www.name.com/api-docs/
  
  # DNS
  # ListRecords
  # response = client.list_records('example.com')
  def list_records(domain)
    connection.get "/domains/#{domain}/records"
  end
  
  # GetRecord
  # response = client.get_record('example.com', '1234')  
  def get_record(domain, id)
    connection.get "/domains/#{domain}/records/#{id}"
  end
  
  # CreateRecord
  # response = client.create_record('example.com', 'www', 'A', '127.0.0.1', 300)
  # response = client.create_record('example.com', 'mail', 'MX', 'mx3.name.com', 300, 10)
  def create_record(domain, host, type, answer, ttl, priority = nil)
    body = {
      'host' => host,
      'type'     => type,
      'answer'  => answer,
      'ttl'      => ttl
    }
    body.update(:priority => priority) if priority
    connection.post "/domains/#{domain}/records", body
  end
  
  # UpdateRecord
  # response = client.update_record('example.com', 'www', 'A', '127.0.0.1', 300, '1234')
  # response = client.update_record('example.com', 'mail', 'MX', 'mx3.name.com', 300, '1234', 10)
  def update_record(domain, host, type, answer, ttl, id, priority = nil)
    body = {
      'host' => host,
      'type'     => type,
      'answer'  => answer,
      'ttl'      => ttl
    }
    body.update(:priority => priority) if priority
    connection.put "/domains/#{domain}/records/#{id}", body
  end
  
  # DeleteRecord
  # response = client.delete_record('example.com', '1234')
  def delete_record(domain, id)
    connection.delete "/domains/#{domain}/records/#{id}"
  end
  
  # DNSSECs
  # ListDNSSECs
  # GetDNSSEC
  # CreateDNSSEC
  # DeleteDNSSEC

  # Domains
  # ListDomains
  def list_domains
    connection.get "/domains"
  end
  
  # GetDomain
  def get_domain(domain)
    connection.get "/domains/#{domain}"
  end

  # CreateDomain
  # Domain is the domain object to create. 
  # contact-types: [ 'registrant','administrative','technical','billing' ]
  # domain = {
  # domainName = 'chirpa.com',
  # nameservers = [ 'ns1.name.com', 'ns2.name.com', 'ns3.name.com' ],
  # contacts = [
  #   { 'type' => 'registrant', 
  #     'first_name'   => 'John',
  #     'last_name'    => 'Doe',
  #     'organization' => 'Name.com',
  #     'address_1'    => '125 Main St',
  #     'address_2'    => 'Suite 300',
  #     'city'         => 'Denver',
  #     'state'        => 'CO',
  #     'zip'          => '80230',
  #     'country'      => 'US',
  #     'phone'        => '+1.3035555555',
  #     'fax'          => '+1.3035555556',
  #     'email'        => 'john@example.net'
  #   }
  # ]
  # privacyEnabled = true,
  # locked = true,
  # autorenewEnabled = true
  # }
  # response = client.create_domain(domain, 12.90)
  def create_domain(domain, purchase_price, purchase_type = 'registration', years = 1)
    options = {
      'domain' => domain,
      'purchasePrice' => purchase_price,
      'purchaseType' => purchase_type,
      'years'      => years
    }

    connection.post '/domains/', options
  end
  
  # EnableAutorenew
  def enable_autorenew(domain)
    connection.post "/domains/#{domain}:enableAutorenew"
  end
  
  # DisableAutorenew
  def disable_autorenew(domain)
    connection.post "/domains/#{domain}:disableAutorenew"
  end
  
  # RenewDomain
  def renew_domain(domain, purchase_price, years = 1)
    options = {
      'purchasePrice' => purchase_price,
      'years' => years
    }
    connection.post "/domains/#{domain}:renew", options
  end
  
  # GetAuthCodeForDomain
  def get_auth_code_for_domain(domain)
    connection.get "/domains/#{domain}:getAuthCode"
  end
    
  # PurchasePrivacy
  def purchase_privacy(domain, purchase_price, years = 1)
    options = {
      'purchasePrice' => purchase_price,
      'years' => years
    }
    connection.post "/domains/#{domain}:purchasePrivacy", options
  end
  
  # SetNameservers
  # response = client.set_nameservers('example.com', [
  #   'ns1.name.com', 'ns2.name.com', 'ns3.name.com'
  # ])
  def set_nameservers(domain, nameservers = {})
    connection.post "/domains/#{domain}:setNameservers", { :nameservers => nameservers }      
  end
  
  # SetContacts
  # response = client.update_domain_contacts('mynewdomain.com', [
  #   { 'type' => [ 'registrant','administrative','technical','billing' ],
  #     'first_name'   => 'John',
  #     'last_name'    => 'Doe',
  #     'organization' => 'Name.com',
  #     'address_1'    => '125 Main St',
  #     'address_2'    => 'Suite 300',
  #     'city'         => 'Denver',
  #     'state'        => 'CO',
  #     'zip'          => '80230',
  #     'country'      => 'US',
  #     'phone'        => '+1.3035555555',
  #     'fax'          => '+1.3035555556',
  #     'email'        => 'john@example.net'
  #   }
  # ])
  def set_contacts(domain, contacts = [])
    connection.post "/domains/#{domain}:setContacts", { :contacts => contacts }
  end
  # LockDomain
  def lock_domain(domain)
    connection.post "/domains/#{domain}:lock"
  end

  # UnlockDomain
  def unlock_domain(domain)
    connection.post "/domains/#{domain}:unlock"
  end  
  
  # CheckAvailability
  def check_availability(domain_names)
    connection.post "/domains:checkAvailability", { domainNames: domainNames}
  end

  # Search
  # response = client.search('example')
  # response = client.search('example', false)
  def search(keyword, tldFilter = true)
    connection.post '/domains:search', {
      'keyword'  => keyword,
      'tldFilter' => tldFilter
    }
  end
  
  # SearchStream
  # response = client.search_stream('example')
  # response = client.search_stream('example', [ 'com', 'net', 'org' ], [ 'availability','suggested' ])
  def search_stream(keyword, tlds = nil, timeout = 1000)
    connection.post '/domains:searchStream', {
      'keyword'  => keyword,
      'tldFilter'     => tlds || [ 'com' ], # ,'net','org','info','us','biz','tel' ],
      'timeout' => services || [ 'availability' ] # ,'suggested' ]
    }
  end

  # EmailForwardings
  # ListEmailForwardings
  # GetEmailForwarding
  # CreateEmailForwarding
  # UpdateEmailForwarding
  # DeleteEmailForwarding

  # Hello
  # HelloFunc
  def hello
    connection.get '/hello'
  end

  # Transfers
  # ListTransfers
  # GetTransfer
  # CreateTransfer
  # CancelTransfer

  # URLForwardings
  # ListURLForwardings
  # GetURLForwarding
  # CreateURLForwarding
  # UpdateURLForwarding
  # DeleteURLForwarding

  # VanityNameservers
  # ListVanityNameservers
  # GetVanityNameserver
  # CreateVanityNameserver
  # UpdateVanityNameserver
  # DeleteVanityNameserver
    
  end

end
